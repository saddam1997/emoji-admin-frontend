
import { authHeader } from '../_helpers';
import { CONST } from '../_config';

export const dashboardService = {
    getAllEmoji,
    addImoji,
    deletedashboardService,
    getAllResourceList,
    saveAssignResourcedashboard,
    getAssignedResourceList,
    udpateStatus,
    deleteEmoji,
    editEmoji,
    uploadImage
};
function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user');
    window.location.href = "#/login"
}

function getAllEmoji(data) {

    let header = new Headers({
        'Content-Type': 'application/json',
        "Authorization": authHeader().Authorization
    });

    const requestOptions = {
        method: "GET",
        headers: header
    }
    let keyWord=``;
    if(data.keyWord){
        keyWord=`&keyWord=${data.keyWord}`;
    }
    return fetch(CONST.BACKEND_URL + `/getAllEmoji?pageNo=${data.pageNo}&size=${data.size}${keyWord}`, requestOptions)
        .then(handleResponse)
        .then(res => {
            let userObj = {
                dashboarddata: res.data
            }
            return userObj;
        });
}
function addImoji(data) {

    let header = new Headers({
        'Content-Type': 'application/json',
        "Authorization": authHeader().Authorization
    });

    const requestOptions = {
        method: "POST",
        headers: header,
        body: JSON.stringify(data)
    }
    return fetch(CONST.BACKEND_URL + `/addEmoji`, requestOptions)
        .then(handleResponse)
        .then(res => {
            let userObj = {
                addImojires: res.data
            }
            return userObj;
        });
}

function uploadImage(filedata) {

    let header = new Headers({
       // 'Content-Type': 'multipart/form-data; boundary=—-WebKitFormBoundaryfgtsKTYLsT7PNUVD',
        "Authorization": authHeader().Authorization
    });
    var data = new FormData();
    data.append('image', filedata);

    const requestOptions = {
        method: "POST",
        headers: header,
        body: data
    }
    return fetch(CONST.BACKEND_URL + `/uploadFile`, requestOptions)
        .then(handleResponse)
        .then(res => {
            let userObj = {
                filesDetails: res.data
            }
            return userObj;
        });
}

function deletedashboardService(data) {
    //console.log("Enter into service ", data);

    let header = new Headers({
        'Content-Type': 'application/json',
        "Authorization": authHeader().Authorization
    });

    const requestOptions = {
        method: "POST",
        headers: header,
        body: JSON.stringify(data)
    }

    return fetch(CONST.BACKEND_URL + `/api/deletedashboard`, requestOptions)
        .then(handleResponse)
        .then(res => {
            let userObj = {
                addImojires: res.data
            }
            return userObj;
        });
}
function getAllResourceList() {

    let header = new Headers({
        'Content-Type': 'application/json',
        "Authorization": authHeader().Authorization
    });
    const requestOptions = {
        method: "POST",
        headers: header
    }
    return fetch(CONST.BACKEND_URL + `/api/listresoures`, requestOptions)
        .then(handleResponse)
        .then(res => {
            //console.log(JSON.stringify(res));

            let userObj = {
                listOfResource: res.data
            }
            return userObj;
        });
}
function getAssignedResourceList(data) {

    let header = new Headers({
        'Content-Type': 'application/json',
        "Authorization": authHeader().Authorization
    });
    const requestOptions = {
        method: "POST",
        headers: header,
        body: JSON.stringify(data)
    }
    return fetch(CONST.BACKEND_URL + `/api/getassignrole`, requestOptions)
        .then(handleResponse)
        .then(res => {
            //console.log(JSON.stringify(res));

            let userObj = {
                listOfAssignedResource: res.data
            }
            return userObj;
        });
}
function saveAssignResourcedashboard(data) {

    let header = new Headers({
        'Content-Type': 'application/json',
        "Authorization": authHeader().Authorization
    });

    const requestOptions = {
        method: "POST",
        headers: header,
        body: JSON.stringify(data)
    }
    return fetch(CONST.BACKEND_URL + `/api/assignrole`, requestOptions)
        .then(handleResponse)
        .then(res => {
            let userObj = {
                updateResource: res
            }
            return userObj;
        });
}
function udpateStatus(data) {
    let header = new Headers({
        'Content-Type': 'application/json',
        "Authorization": authHeader().Authorization
    });
    const requestOptions = {
        method: "POST",
        headers: header,
        body: JSON.stringify(data)
    }
    return fetch(CONST.BACKEND_URL + `/updateStatus`, requestOptions)
        .then(handleResponse)
        .then(res => {
            let userObj = {
                updatestatus: res.data
            }
            return userObj;
        });
}


function deleteEmoji(data) {

    let header = new Headers({
        'Content-Type': 'application/json',
        "Authorization": authHeader().Authorization
    });
    const requestOptions = {
        method: "POST",
        headers: header,
        body: JSON.stringify(data)
    }
    return fetch(CONST.BACKEND_URL + `/deleteEmoji`, requestOptions)
        .then(handleResponse)
        .then(res => {
            let userObj = {
                updatestatus: res.data
            }
            return userObj;
        });
}

function editEmoji(data) {

    let header = new Headers({
        'Content-Type': 'application/json',
        "Authorization": authHeader().Authorization
    });
    const requestOptions = {
        method: "POST",
        headers: header,
        body: JSON.stringify(data)
    }
    return fetch(CONST.BACKEND_URL + `/editEmoji`, requestOptions)
        .then(handleResponse)
        .then(res => {
            let userObj = {
                updatestatus: res.data
            }
            return userObj;
        });
}

//editEmoji
function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                logout();
            }
            const error = (data && data.msg) || response.statusText;
            return Promise.reject(error);
        }
        if (data.error) {
            const error = (data && data.msg) || response.statusText;
            return Promise.reject(error);
        }
        //console.log("datadatadata ", data);

        return data;
    });
}