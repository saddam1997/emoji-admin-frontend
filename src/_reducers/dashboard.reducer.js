import { dashboardConstants } from '../_constants';

export function dashboard(state = {}, action) {
  //console.log("action.typeaction.typeaction.type  ", action);

  switch (action.type) {
    case dashboardConstants.GETALL_DASHBOARD_REQUEST:
      return {
        loading: true,
        items: state.items
      };
    case dashboardConstants.GETALL_DASHBOARD_SUCCESS:
      return {
        dashboarddata: action.dashboards.dashboarddata,
      };
    case dashboardConstants.GETALL_DASHBOARD_FAILURE:
      return {
        error: action.error
      };


    case dashboardConstants.ADD_IMOJI_STATUS_REQUEST:
      return {
        loading: true,
        dashboarddata: state.dashboarddata
      };
    case dashboardConstants.ADD_IMOJI_STATUS_SUCCESS:
      return {
        dashboarddata: state.dashboarddata, emojiAdded: true

      };
    case dashboardConstants.ADD_IMOJI_STATUS_FAILURE:
      return {
        error: action.error
      };

    case dashboardConstants.UPDATE_STATUS_EMOJI_REQUEST:
      return {
        loading: true,
        dashboarddata: state.dashboarddata
      };
    case dashboardConstants.UPDATE_STATUS_EMOJI_SUCCESS:
      return {
        dashboarddata: state.dashboarddata, emojiAdded: true
      };
    case dashboardConstants.UPDATE_STATUS_EMOJI_FAILURE:
      return {
        error: action.error
      };


    case dashboardConstants.DELETE_EMOJI_REQUEST:
      return {
        loading: true,
        dashboarddata: state.dashboarddata
      };
    case dashboardConstants.DELETE_EMOJI_SUCCESS:
      return {
        dashboarddata: state.dashboarddata, emojiAdded: true
      };
    case dashboardConstants.DELETE_EMOJI_FAILURE:
      return {
        error: action.error
      };


      case dashboardConstants.EDIT_EMOJI_REQUEST:
        return {
          loading: true,
          dashboarddata: state.dashboarddata
        };
      case dashboardConstants.EDIT_EMOJI_SUCCESS:
        return {
          dashboarddata: state.dashboarddata, emojiAdded: true
        };
      case dashboardConstants.EDIT_EMOJI_FAILURE:
        return {
          error: action.error
        };




    case dashboardConstants.FILE_UPLOAD_STATUS_SUCCESS:
      return {
        ...state, filesDetails: action.uploadImage.filesDetails,
      };

    case dashboardConstants.FILE_UPLOAD_STATUS_FAILURE:
      return {
        ...state
      };
    case dashboardConstants.SUB_ADMIN_DELETE_SUCCESS:
      return {
        ...state, isAdminDeleted: true
      };
    case dashboardConstants.SUB_ADMIN_DELETE_FAILURE:
      return {
        ...state
      };

    case dashboardConstants.GET_RESOUCE_SUCCESS:
      return {

        listOfResource: action.resources.listOfResource,
        items: state.items,
        total: state.total,

      };
    case dashboardConstants.GET_RESOUCE_FAILURE:
      return {
        error: action.error
      };

    case dashboardConstants.SAVE_ASSIGNED_RESOUCE_SUCCESS:
      return {
        ...state, isAssignedResource: true
      };
    case dashboardConstants.SAVE_ASSIGNED_RESOUCE_FAILURE:
      return {
        ...state
      };
    case dashboardConstants.GET_ASSIGNED_RESOUCE_SUCCESS:
      return {
        listOfAssignedResource: action.assignedresources.listOfAssignedResource,
        items: state.items,
        total: state.total
      };
    case dashboardConstants.GET_ASSIGNED_RESOUCE_FAILURE:
      return {
        error: action.error,
        listOfAssignedResource: [],
        ...state
      };

    case dashboardConstants.UPDATE_STATUS_REQUEST:
      return {
        loading: true,
        items: state.items,
        listOfResource: state.listOfResource,
        total: state.total
      };
    case dashboardConstants.UPDATE_STATUS_SUCCESS:
      return {
        isStatusUpdated: true,
        items: state.items,
        listOfResource: state.listOfResource,
        total: state.total
      };
    case dashboardConstants.UPDATE_STATUS_FAILURE:
      return {
        error: action.error,
        items: state.items,
        listOfResource: state.listOfResource,
        total: state.total
      };
    default:
      return state
  }
}