import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, 
Row, Button ,FormGroup,Input,
Modal,
ModalHeader,
ModalBody,
ModalFooter

} from 'reactstrap';
//import Switch from 'react-switch';
import { CONST } from '../../../_config';
import moment from 'moment'

import { connect } from 'react-redux';
import { userActions } from '../../../_actions';
const statusOption = [{ value: 1, name: "Active" },{ value: 0, name: "Deactive" }];
class ViewUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      addPoolModal: false,
      addPrizeModal: false,
      showPoolModal: false,
      editPoolModal: false,
      showPrizeModal:false,
      selectedPage: 1,
      totalpage: 0,
      checked: false,
      combined: false,
      single: false,
      multiple: false,
      addPoolStatus:statusOption[0].value,
      shareholders: [{ name: "" }],
      isEdit:false,
    };
    this.showImageToggle = this.showImageToggle.bind(this);

    this.handleChangeInput = this.handleChangeInput.bind(this);
    this.udpateUserKYC = this.udpateUserKYC.bind(this);
    this.udpateUserStatus = this.udpateUserStatus.bind(this);
    this.handleRemoveShareholder = this.handleRemoveShareholder.bind(this);
    this.showPrizeToggle = this.showPrizeToggle.bind(this);
    this.handleChangeAddPool1 = this.handleChangeAddPool1.bind(this);
    this.addPrizeToggle = this.addPrizeToggle.bind(this);
    this.addPoolToggle = this.addPoolToggle.bind(this);
    this.handleChangeAddPool = this.handleChangeAddPool.bind(this);
    this.showPrizeClose = this.showPrizeClose.bind(this);
    this.handleOptionStatus = this.handleOptionStatus.bind(this);
    this.handleParameterKeyValueChange = this.handleParameterKeyValueChange.bind(this);
  }
  componentDidMount() {
    console.log("this.props.match.params.id   ",this.props.match.params.id);
    let data={
      userid:this.props.match.params.id
    }
    this.props.dispatch(userActions.getAllInfo(data));
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.users.kycStatusUpdate) {
      let data={
        userid:this.props.match.params.id
      }
      this.props.dispatch(userActions.getAllInfo(data));
    }
    if (nextProps.users.detailsUpdate) {
      let data={
        userid:this.props.match.params.id
      }
      this.props.dispatch(userActions.getAllInfo(data));
      this.setState({isEdit:!this.state.isEdit})
    }
    if (nextProps.users.prizeAdded) {
      this.setState({ addPrizeModal: false });
    }
    if (nextProps.users.prizeList) {
      this.setState({ prizeList: nextProps.users.prizeList ,showPrizeModal: !this.state.showPrizeModal});
    }
    if (nextProps.users.userInfo) {
      this.setState({ userInfo: nextProps.users.userInfo});
    }
    if (nextProps.users.isUpdate) {
       let data={
        userid:this.props.match.params.id
      }
      this.props.dispatch(userActions.getAllInfo(data));
    }
  }
  getBadge = status => {
    return status === true ? 'success' : 'danger';
  }
  addPoolToggle() {
    this.setState({
      addPoolModal: !this.state.addPoolModal
    });
  }
  showPrizeToggle(pool) {
    console.log("poolpool  ",pool);
    this.setState({
      prizeList:[],
      
    });
    if (pool.id) {
       let data={
          poolcontestid:pool.id
        }
        this.props.dispatch(userActions.getPrize(data));
    }
  }
  showPrizeClose(pool) {
    console.log("poolpool  ",pool);
    this.setState({
      showPrizeModal:false
    });
  }
  handleChangeAddPool(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }
  handleChangeAddPool1(name, value) {
   console.log('name  ',name);
   console.log('value  ',value);
   
    this.setState({ [name]: value });
  }
  handleOptionStatus(e) {
    const { name, value } = e.target;
   console.log(name +"  "+value);
    this.setState({addPoolStatus: value });
  }
  addPool() {
    // console.log("this.state.pooljoinfee  ",this.state.pooljoinfee);
    // console.log("this.state.pooltotalwining  ",this.state.pooltotalwining);
    // console.log("this.state.poolwinners  ",this.state.poolwinners);
    // console.log("this.state.poolmaxteams  ",this.state.poolmaxteams);
    // console.log("this.state.addPoolStatus  ",this.state.addPoolStatus);
    // console.log("this.state.combined  ",this.state.combined);
    // console.log("this.state.single  ",this.state.single);
    // console.log("this.state.multiple  ",this.state.multiple);
        
    let data={
      contestid:this.props.match.params.id,
      joinfee:this.state.pooljoinfee,
      totalwining:this.state.pooltotalwining,
      winners:this.state.poolwinners,
      maxteams:this.state.poolmaxteams,
      c:this.state.combined?1:0,
      m:this.state.multiple?1:0,
      s:this.state.single?1:0,
      status:this.state.addPoolStatus
    };
    console.log(data);
    
    this.props.dispatch(userActions.addPool(data));
  }
  addPrizeToggle(pool) {
    console.log(pool);
    this.setState({
      poolid:pool.id,
      shareholders:[],
      addPrizeModal: !this.state.addPrizeModal
    });
  }
  handleOptionStatusPrize(e) {
    const { name, value } = e.target;
   console.log(name +"  "+value);
    this.setState({addPrizeStatus: value });
  }
  addPrize() {
    console.log("this.state.pooljoinfee  ",this.state.shareholders);
    
    let data={
      poolcontestid:this.state.poolid,
      prizekeyvalue:this.state.shareholders,
    };
   this.props.dispatch(userActions.addPrize(data));
  }
  handleParameterKeyValueChange = idx => evt => {
    const newShareholders = this.state.shareholders.map((shareholder, sidx) => {
      if (idx !== sidx) return shareholder;
      console.log("evt.target.name   ",evt.target.name);
      return { ...shareholder, [evt.target.name]: evt.target.value };
    });
    console.log("newShareholders  ",newShareholders);
    this.setState({ shareholders: newShareholders });
  }
  handleAddPrizeBreaker = () => {
    this.setState({
      shareholders: this.state.shareholders.concat([{ pmin: '',pmax: '',pamount: '' }])
    });
  }
  handleRemoveShareholder = idx => () => {
    this.setState({
      shareholders: this.state.shareholders.filter((s, sidx) => idx !== sidx)
    });
  }
  udpateUserStatus = (user,status) => {
    console.log(user);
    console.log(status);
    let data={
      userid:user.id,
      status:status
    }
    this.props.dispatch(userActions.updateUserStatus(data));
  }
  udpateUserKYC = (user,type) => {
    console.log(user);
    console.log(type);
    let data={
      userid:user.id,
      atype:type,
      status:1
    }
    console.log(data);
    this.props.dispatch(userActions.udpateUserKYC(data));
  }

  
  handleChangeInput(e) {
    const { value } = e.target;
    console.log('asdfadf  ',value);

    var userInfo = {...this.state.userInfo};
    console.log(userInfo);
    
    userInfo.name = value;
    this.setState({userInfo})
  }
  updateUserInfo(userinfo) {

    let data={
      name:this.state.userInfo.name,
      userid:userinfo.id,
    };
    console.log(data);
    
   this.props.dispatch(userActions.udpateUserDetails(data));
  }
    //Delete Dialog box
  showImageToggle(image) {
    if (image) {
       this.setState({
          showImageModal: !this.state.showImageModal,
          imageURL:image
        });
    } else {
       this.setState({
          showImageModal: !this.state.showImageModal,
          imageURL:''
        });
    }
   
  }

  render() {

    const {users}=this.props;
    const {userInfo}=users;
    console.log("CONST.BACKEND_URL  ",CONST.BACKEND_URL);
    
    return (
      <div className="animated fadeIn">
        <Row>
          <Col lg={12}>
            <Card>
              <CardHeader>
                  <FormGroup row>
                          <Col xl="3"><strong><i className="icon-info pr-1"></i>ViewUser id: {this.props.match.params.id}</strong></Col>
                          <Col xl="3">
                            <Button className="btn-sm btn-square btn-success"  onClick={() => this.udpateUserKYC(userInfo,'bank')}>Bank Verified</Button>&nbsp;&nbsp;&nbsp;
                            <Button className="btn-sm btn-square btn-success"  onClick={() => this.udpateUserKYC(userInfo,'pancard')}>PAN Verified</Button>&nbsp;&nbsp;&nbsp;
                            {/* <Button className="btn-sm btn-square btn-success"  onClick={() => this.setState({isEdit:!this.state.isEdit})}>&nbsp;&nbsp;&nbsp;Edit&nbsp;&nbsp;&nbsp;</Button>&nbsp;&nbsp;&nbsp; */}
                            
                          </Col>
                          {userInfo?
                          <Col xl="2">
                          {/* <Button className="btn-sm btn-square btn-success"  onClick={() => this.udpateUserKYC(userInfo,2)}>Phone Verified</Button>&nbsp;&nbsp;&nbsp; */}
                          
                          { userInfo.status === "0" || userInfo.status === "2" ?<span><Button className="btn-sm btn-square btn-success" onClick={() => this.udpateUserStatus(userInfo,1)}>Active&nbsp;&nbsp;&nbsp;</Button> &nbsp;</span>:null} 
                          { userInfo.status === "1" || userInfo.status === "2" ?<span><Button className="btn-sm btn-square btn-danger"  onClick={() => this.udpateUserStatus(userInfo,0)}>Inactive</Button>&nbsp;</span>:null}
                          { userInfo.status === "1" || userInfo.status === "0" ?<span><Button className="btn-sm btn-square btn-danger"  onClick={() => this.udpateUserStatus(userInfo,2)}>Block&nbsp;&nbsp;&nbsp;</Button>&nbsp;</span>:null}
                          </Col>:null}
                          <Col xl="3">
                          <span><Button className="btn-sm btn-square btn-danger" onClick={() => this.udpateUserKYC(userInfo,"bankreject")}>Bank Reject</Button>&nbsp;&nbsp;&nbsp;</span>
                          <span><Button className="btn-sm btn-square btn-danger" onClick={() => this.udpateUserKYC(userInfo,"panreject")}>PAN Reject</Button></span>
                          </Col>
                    
                  </FormGroup>
              </CardHeader>
              <CardBody>
                  {
                   userInfo? 
                   <Row>
                    <Col xs="3">
                      Team Name
                      </Col>
                      <Col xs="3" className="for_padding_bottom">
                        <Input
                          type="text"
                          name="name"
                          id="name"
                          autoComplete="off"
                          value={userInfo.teamname}
                          onChange={this.handleChangeInput}
                          disabled={!this.state.isEdit}
                        />
                      </Col>
                      {/* {
                        this.state.isEdit?<Col xs="6">
                          <Button className="btn-sm btn-square btn-success"  onClick={() => this.updateUserInfo(userInfo)}>Update</Button>
                        </Col>:<Col xs="6"/>
                      
                      } */}
                      <Col xs="6"/>
                      <Col xs="3">
                      dob
                      </Col>
                      <Col xs="3" className="for_padding_bottom">
                        <Input
                          type="text"
                          name="username"
                          id="username"
                          autoComplete="off"
                          value={userInfo.dob?moment(new Date(parseInt(userInfo.dob)*1000)).utcOffset("+05:30").format("YYYY-MM-DD"):'DOB not available'}
                          onChange={this.handleChangeAddSubAdmin}
                          disabled
                        />
                      </Col>
                      <Col xs="6"/>

                      <Col xs="3">
                      gender
                      </Col>
                      <Col xs="3" className="for_padding_bottom">
                        <Input
                          type="text"
                          name="username"
                          id="username"
                          autoComplete="off"
                          value={userInfo.gender}
                          onChange={this.handleChangeAddSubAdmin}
                          disabled
                        />
                      </Col>
                      <Col xs="6"/>

                      <Col xs="3">
                      acholdername
                      </Col>
                      <Col xs="3" className="for_padding_bottom">
                        <Input
                          type="text"
                          name="username"
                          id="username"
                          autoComplete="off"
                          value={userInfo.acholdername}
                          onChange={this.handleChangeAddSubAdmin}
                          disabled
                        />
                      </Col>
                      <Col xs="6"/>

                      <Col xs="3">
                      acno
                      </Col>
                      <Col xs="3" className="for_padding_bottom">
                        <Input
                          type="text"
                          name="username"
                          id="username"
                          autoComplete="off"
                          value={userInfo.acno}
                          onChange={this.handleChangeAddSubAdmin}
                          disabled
                        />
                      </Col>
                      <Col xs="6"/>


                      <Col xs="3">
                      bankname
                      </Col>
                      <Col xs="3" className="for_padding_bottom">
                        <Input
                          type="text"
                          name="username"
                          id="username"
                          autoComplete="off"
                          value={userInfo.bankname}
                          onChange={this.handleChangeAddSubAdmin}
                          disabled
                        />
                      </Col>
                      <Col xs="6"/>


                      <Col xs="3">
                      ifsccode
                      </Col>
                      <Col xs="3" className="for_padding_bottom">
                        <Input
                          type="text"
                          name="username"
                          id="username"
                          autoComplete="off"
                          value={userInfo.ifsccode}
                          onChange={this.handleChangeAddSubAdmin}
                          disabled
                        />
                      </Col>
                      <Col xs="6"/>


                      <Col xs="3">
                      isbankdverify
                      </Col>
                      <Col xs="3" className="for_padding_bottom">
                        <Input
                          type="text"
                          name="username"
                          id="username"
                          autoComplete="off"
                          value={userInfo.isbankdverify ==='1'? 'Verified':'Not Verified'}
                          onChange={this.handleChangeAddSubAdmin}
                          disabled
                        />
                      </Col>
                      <Col xs="6"/>


                      <Col xs="3">
                      pannumber
                      </Col>
                      <Col xs="3" className="for_padding_bottom">
                        <Input
                          type="text"
                          name="username"
                          id="username"
                          autoComplete="off"
                          value={userInfo.pannumber}
                          onChange={this.handleChangeAddSubAdmin}
                          disabled

                        />
                      </Col>
                      <Col xs="6"/>


                      <Col xs="3">
                      panname
                      </Col>
                      <Col xs="3" className="for_padding_bottom">
                        <Input
                          type="text"
                          name="username"
                          id="username"
                          autoComplete="off"
                          value={userInfo.panname}
                          onChange={this.handleChangeAddSubAdmin}
                          disabled
                        />
                      </Col>
                      <Col xs="6"/>

                      <Col xs="3">
                      pandob
                      </Col>
                      <Col xs="3" className="for_padding_bottom">
                        <Input
                          type="text"
                          name="username"
                          id="username"
                          autoComplete="off"
                          value={(userInfo.pandob)?moment(new Date(parseInt(userInfo.pandob)*1000)).utcOffset("+05:30").format("YYYY-MM-DD"):""}
                          
                          onChange={this.handleChangeAddSubAdmin}
                          disabled
                        />
                      </Col>
                      <Col xs="6"/>


                      <Col xs="3">
                      panimage
                      </Col>
                      <Col xs="3" className="for_padding_bottom">
                      <img
                            src={userInfo.panimage===null || userInfo.panimage===""?'images/pan_dummy.png':userInfo.panimage}
                            width="100"
                            height="100"
                            style={{cursor:'pointer'}}
                            onClick={() =>
                                  this.showImageToggle(userInfo.panimage)
                            }
                            alt=""
                          />
                        {/* <Input
                          type="text"
                          name="username"
                          id="username"
                          autoComplete="off"
                          value={userInfo.panimage}
                          onChange={this.handleChangeAddSubAdmin}
                          disabled
                        /> */}
                      </Col>
                      <Col xs="6"/>


                      <Col xs="3">
                      ispanverify
                      </Col>
                      <Col xs="3" className="for_padding_bottom">
                        <Input
                          type="text"
                          name="username"
                          id="username"
                          autoComplete="off"
                          value={userInfo.ispanverify ==='1'? 'Verified':'Not Verified'}
                          
                          onChange={this.handleChangeAddSubAdmin}
                          disabled
                        />
                      </Col>
                      <Col xs="6"/>


                      <Col xs="3">
                      created
                      </Col>
                      <Col xs="3" className="for_padding_bottom">
                        <Input
                          type="text"
                          name="username"
                          id="username"
                          autoComplete="off"
                          value={moment(new Date(parseInt(userInfo.created)*1000)).utcOffset("+05:30").format("YYYY-MM-DD")}
                          onChange={this.handleChangeAddSubAdmin}
                          disabled
                        />
                      </Col>
                      <Col xs="6"/>


                      <Col xs="3">
                      profilepic
                      </Col>
                      <Col xs="3" className="for_padding_bottom">
                       <img
                            src={userInfo.profilepic===null ||userInfo.profilepic==='' ? 'images/user.png':userInfo.profilepic}
                            width="50"
                            height="50"
                            onClick={() =>this.showImageToggle(userInfo.profilepic)}
                            style={{cursor:'pointer'}}
                            onError={(e)=>{e.target.src=CONST.BACKEND_URL + '/uploads/icons/dummy.png'}}
                            className="img-avatar"
                            alt=""
                          />
                      </Col>
                      <Col xs="6"/>



                      <Col xs="3">
                      logindate
                      </Col>
                      <Col xs="3" className="for_padding_bottom">
                        <Input
                          type="text"
                          name="username"
                          id="username"
                          autoComplete="off"
                          value={(userInfo.logindate && userInfo.logindate!=="0")?moment(new Date(parseInt(userInfo.logindate)*1000)).utcOffset("+05:30").format("YYYY-MM-DD hh:mm:ss"):""}
                          onChange={this.handleChangeAddSubAdmin}
                          disabled
                        />
                      </Col>
                      <Col xs="6"/>


                      {/* <Col xs="3">
                      logintype
                      </Col>
                      <Col xs="3" className="for_padding_bottom">
                        <Input
                          type="text"
                          name="username"
                          id="username"
                          autoComplete="off"
                          value={userInfo.logintype}
                          onChange={this.handleChangeAddSubAdmin}
                          disabled
                        />
                      </Col>
                      <Col xs="6"/> */}
                      {/* <Col xs="3">
                      devicetype 
                      </Col>
                      <Col xs="3" className="for_padding_bottom">
                        <Input
                          type="text"
                          name="username"
                          id="username"
                          autoComplete="off"
                          value={userInfo.devicetype}
                          onChange={this.handleChangeAddSubAdmin}
                          disabled
                        />
                        
                      </Col> */}
                      
                      <Col xs="6"/>


                    </Row>
                    :null
                  }
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Modal isOpen={this.state.showImageModal} toggle={this.showImageToggle} className={this.props.className}>
          <ModalHeader toggle={this.showImageToggle}>
              Image
          </ModalHeader>
          <ModalBody>
              {this.state.imageURL?<img src={this.state.imageURL} width="400" height="400"  alt=""/>:"Image not found."}
          </ModalBody>
          <ModalFooter>
              <Button color="secondary" onClick={this.showImageToggle}>
                  Cancel
              </Button>
              
          </ModalFooter>
      </Modal>
     
      </div>
    )
  }
}
function mapStateToProps(state) {
  const { users} = state;
  
  return {
    users
  };
}
export default connect(mapStateToProps)(ViewUser);