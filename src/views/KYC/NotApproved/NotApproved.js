import React, { Component } from 'react';

import { Card, CardBody, CardHeader, Col, Row, Table, Badge,
 FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  Button,
 } from 'reactstrap';
import PaginationComponent from "react-reactstrap-pagination";
import { connect } from 'react-redux';
import { userActions } from '../../../_actions';
import moment from 'moment'


class NotApproved extends Component {
   constructor(props) {
    super(props);
    this.state = {
       totalpage: 0,
       selectedPage:0
    }
    this.udpateUserStatus = this.udpateUserStatus.bind(this);
    this.handleSelectedPaginate = this.handleSelectedPaginate.bind(this);
    this.handleChangeSearch = this.handleChangeSearch.bind(this);
  }
  componentDidMount() {
    console.log("componentDidMount");
    let data={
      atype:'kycnonapproved'
    }
    this.props.dispatch(userActions.getAll(data));
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.users.total >= 0) {
      this.setState({ totalpage: nextProps.users.total })
    }
    if (nextProps.users.isUpdate) {
       let data = {
          "limit": 10,
          "page": this.state.selectedPage,
          "search": '',
          atype:'kycnonapproved'
        }
        this.props.dispatch(userActions.getAll(data));
    }
  }
  getBadge = (status) => {
    return status === "1" ? 'success' :
      'danger'
  }
  handleSelectedPaginate(selectedPage) {
    let data = {
      limit: 10,
      page: selectedPage,
      search: '',
      atype:'kycnonapproved'
    }
    this.setState({ selectedPage: selectedPage })
  

    this.props.dispatch(userActions.getAll(data));
  }
  handleChangeSearch(e) {
    const { value } = e.target;
    let data = {
      "limit": 10,
      "page": 1,
      "search": value.replace(/^\s+|\s+$/g, ''),
      atype:'kycnonapproved'
    }
    this.props.dispatch(userActions.getAll(data));
  }
  udpateUserStatus = (user,status) => {
    console.log(user);
    console.log(status);
    let data={
      userid:user.id,
      status:status
    }
    this.props.dispatch(userActions.updateUserStatus(data));
  }
  render() {

    const { users } = this.props;
    let { items } = users;

    return (
      <div className="animated fadeIn">
        <Row>
          <Col xl={12}>
            <Card>
              <CardHeader>
                <FormGroup row>
                  <Col xl="6">
                    <i className="fa fa-align-justify"></i> User <small className="text-muted">List</small>
                  </Col>
                  <Col xl="3"></Col>
                  <Col xl="3">
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <Button type="button" color="primary" ><i className="fa fa-search"></i></Button>
                      </InputGroupAddon>
                      <Input type="text" id="search" name="search" placeholder="Email" onChange={this.handleChangeSearch} autoComplete="off" />
                    </InputGroup>
                  </Col>
                </FormGroup>
              </CardHeader>
              <CardBody>
                <Table responsive striped>
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">PHONE</th>
                      <th scope="col">EMAIL</th>
                      <th scope="col">REG. DATE</th>
                      <th scope="col">PHONE VERIFY</th>
                      <th scope="col">EMAIL VERIFY</th>
                      <th scope="col">PAN VERIFY</th>
                      <th scope="col">BANK VERIFY</th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      items ? items.map((user, index) => <tr key={user.id}>
                        <td>{this.state.selectedPage===0 ||this.state.selectedPage===1?(index + 1):((this.state.selectedPage-1)*10) + (index + 1)}</td>
                        <td><a block color="link" href={`#/users/allusers/${user.id}`}>{user.phone}</a></td>
                        <td><a block color="link" href={`#/users/allusers/${user.id}`}>{user.email}</a></td>
                        <td>{moment(new Date(parseInt(user.created)*1000)).utcOffset("+05:30").format("YYYY-MM-DD")}</td>
                        <td><Badge color={this.getBadge(user.isphoneverify)} >{user.isphoneverify==="0"?"Not Verify":"Verified"}</Badge></td>
                        <td><Badge color={this.getBadge(user.isemailverify)} >{user.isemailverify==="0"?"Not Verify":"Verified"}</Badge></td>
                        <td><Badge color={this.getBadge(user.ispanverify)} >{user.ispanverify==="0"?"Not Verify":"Verified"}</Badge></td>
                        <td><Badge color={this.getBadge(user.isbankdverify)} >{user.isbankdverify==="0"?"Not Verify":"Verified"}</Badge></td>
                      </tr >
                      ) : null
                    }
                  </tbody>
                </Table>
                {
                parseInt(this.state.totalpage) > 10 ?
                 (<PaginationComponent totalItems={parseInt(this.state.totalpage)} pageSize={10} onSelect={this.handleSelectedPaginate} />) 
                 : (null)
                }
              </CardBody>
            </Card>
          </Col>
        </Row>
       
      </div>
    )
  }
}
function mapStateToProps(state) {
  const { users, authentication } = state;
  const { user } = authentication;
  return {
    user,
    users
  };
}
export default connect(mapStateToProps)(NotApproved);

