import React, { Component } from 'react';
import { dashboardActions } from '../../_actions';

//import CustomRow from '../../_components/CustomRow';
import {
  Row,
  Table,
  Col,
  Card,
  CardBody,
  Modal,
  ModalBody,
  ModalHeader,
  Button,
  Input,CardHeader,FormGroup,InputGroupAddon,
  Form,
  InputGroup,
} from 'reactstrap';
import { connect } from 'react-redux';
import PaginationComponent from 'react-reactstrap-pagination';

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
    this.resetFile = this.resetFile.bind(this);
    this.dropdownToggle = this.dropdownToggle.bind(this);
    this.addEmoji = this.addEmoji.bind(this);
    this.handleChangeEdited = this.handleChangeEdited.bind(this);
    this.editEmoji = this.editEmoji.bind(this);
    this.handleSelectedPaginate = this.handleSelectedPaginate.bind(this);
    this.editEmojiData = this.editEmojiData.bind(this);
    this.handleChangeSearch = this.handleChangeSearch.bind(this);
    this.handleChangeSearchEmoji = this.handleChangeSearchEmoji.bind(this);
    this.showImageEmoji = this.showImageEmoji.bind(this);

    this.state = {
      file: null,
      imojiName: '',
      keyWord: '',
      selectedPage: 0,
      filesDetails1: {}
    };
  }
  componentDidMount() {
    //console.log("componentDidMount");
    let data={
      pageNo:1,
      size:10
    }
    this.props.dispatch(dashboardActions.getAllEmoji(data));
  }
  componentWillUnmount() {
    
  }
  
  static getDerivedStateFromProps(props, state) {
    
    if (props.dashboard.filesDetails) {
      return {
        filesDetails1: props.dashboard.filesDetails
      };
    }
    if (props.dashboard.emojiAdded === true) {
      return {
        modal1: false,
        modal2: false
      }
    }
    //console.log("getDerivedStateFromProps ::: ",{...props.dashboard.dashboarddata, ...state});

    return {...props.dashboard.dashboarddata, ...state} ;
  }
  componentDidUpdate(prevProps) {
    if (prevProps.dashboard.emojiAdded === true) {
      let data={
        pageNo:this.state.selectedPage===0?1:this.state.selectedPage,
        size:10
      }
      this.props.dispatch(dashboardActions.getAllEmoji(data));
    }
  }
 
  handleChangeSearch(e) {
    const { value } = e.target;
    this.setState({ imojiName: value });
  }
  handleChangeSearchEmoji(e) {
    const { value } = e.target;
    this.setState({ keyWord: value });
    let data={
      pageNo:this.state.selectedPage===0?1:this.state.selectedPage,
      keyWord:value,
      size:10
    }
    this.props.dispatch(dashboardActions.getAllEmoji(data));
  }
  handleChangeEdited(e) {
    const { value } = e.target;
    this.setState({ "editText": value });
  }
  onChange(event) {
    this.setState({
      file: URL.createObjectURL(event.target.files[0])
    });
    this.props.dispatch(dashboardActions.uploadImage(event.target.files[0]));
  }
  dropdownToggle() {
    this.setState({
      dd1: !this.state.dd1
    });
  }
  closeModal(tabId) {
    this.setState({
      [tabId]: false,
    });
  }
  showModal(modal) {
    this.setState({
      [modal]: true,
      file: null
    });
  }
  addEmoji() {
    let data = {
      imageFileName: this.state.filesDetails1.uploadedImageName,
      imojiName: this.state.imojiName,
    }
    this.props.dispatch(dashboardActions.addImoji(data));
  }
  updateStatus(data) {
    let data1 = {
      id: data.id
    }
    
    this.props.dispatch(dashboardActions.udpateStatus(data1));
  }
  handleSelectedPaginate(selectedPage) {
    let data={
      pageNo:selectedPage,
      size:10
    }
    this.setState({ selectedPage: selectedPage })
    this.props.dispatch(dashboardActions.getAllEmoji(data));
  }

  deleteEmoji(data) {
    let data1 = {
      id: data.id
    }
    this.props.dispatch(dashboardActions.deleteEmoji(data1));
  }

  editEmoji(row) {
    this.setState({
      "rowId":row.id,
      "editText":row.imojiName
    });
    this.showModal('modal2');
  }

  showImageEmoji(row) {
    this.setState({
      "showImageURL":row.imageURL
    });
    this.showModal('modal3');
  }

  editEmojiData() {

    let data = {
      id: this.state.rowId,
      imojiName: this.state.editText,
    }
    this.props.dispatch(dashboardActions.editEmoji(data));
  
  }

  resetFile(event) {
    event.preventDefault();
    this.setState({ file: null });
  }
  loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

  render() {
    const { dashboard } = this.props;
    let { dashboarddata } = dashboard;

    return (
      <div className="animated fadeIn ">
        <Row>
          <Col xl={12}>
            <Card><CardHeader>
                <FormGroup row>
                  <Col xl="6">
                    <i className="fa fa-align-justify" /> Emoji{' '}
                    <small className="text-muted">List</small>
                  </Col>
                  <Col xl="3">
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <Button type="button" color="primary">
                          <i className="fa fa-search" />
                        </Button>
                      </InputGroupAddon>
                      <Input
                        type="text"
                        id="search"
                        name="search"
                        placeholder="Search Emoji"
                        onChange={this.handleChangeSearchEmoji}
                        autoComplete="off"
                      />
                    </InputGroup>
                  </Col>
                  <Col xl="1" />
                  <Col xl="2">
                    <Button
                      onClick={this.showModal.bind(this, 'modal1')}
                      className="mr-1"
                      color="success"
                    >
                       Add Emoji
                    </Button>
                  </Col>
                </FormGroup>
              </CardHeader>
              <CardBody>
                {/* <Button onClick={this.showModal.bind(this, 'modal1')}>
                  Add Emoji
                </Button> */}
                <Table striped>
                  <thead>
                    <tr>
                      <th>S. No.</th>
                      <th>Image</th>
                      <th>Name</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      dashboarddata
                      ? dashboarddata.list.map((imojiData, index) => (
                        // <CustomRow 
                        // key={index} 
                        // imojiData={imojiData}
                        // updateStatus={this.updateStatus}
                        // index={this.state.selectedPage===0 ||this.state.selectedPage===1?(index + 1):((this.state.selectedPage-1)*10) + (index + 1)}
                        // />
                        <tr key={index}>
                          <th scope="row">{this.state.selectedPage===0 ||this.state.selectedPage===1?(index + 1):((this.state.selectedPage-1)*10) + (index + 1)}</th>
                          <td><img src={imojiData.imageURL} style={{height:'50px',width:'50px'}} 
                          onClick={()=>this.showImageEmoji(imojiData)}
                          className="img-avatar" alt="bootstrapmaster" /></td>
                          <td>{imojiData.imojiName}</td>
                          <td>
                            {
                              imojiData.isEnable?
                              <Button color="danger" onClick={()=>this.updateStatus(imojiData)} style={{marginRight:'10px'}}>
                                Disable</Button>:
                              <Button color="success" onClick={()=>this.updateStatus(imojiData)} style={{marginRight:'10px'}}>Enable</Button>
                            }
                            {'  '}
                            <Button color="warning" 
                              onClick={()=>this.editEmoji(imojiData)}
                              style={{marginRight:'10px'}}>Edit</Button>
                            {'  '}
                            <Button color="danger" onClick={()=>this.deleteEmoji(imojiData)} style={{marginRight:'10px'}}>Delete</Button>
                          </td>
                        </tr>
                       ))
                      : null}
                  </tbody>
                </Table>
                
                {this.state.totalPage && this.state.totalPage > 10 ? (
                  <PaginationComponent
                    totalItems={parseInt(this.state.totalPage)}
                    pageSize={10}
                    onSelect={this.handleSelectedPaginate}
                  />
                ) : null}
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Modal isOpen={this.state.modal1} toggle={this.closeModal.bind(this, 'modal1')}>
          <ModalHeader toggle={this.closeModal.bind(this, 'modal1')}>
            Add New Emoji
           </ModalHeader>
          <ModalBody>
            <Row className="justify-content-center">
              <Col md="8">
                <Form name="form" onSubmit={this.handleSubmit}>
                  <InputGroup className="mb-3">
                    <Input type="file" onChange={this.onChange} />
                    {this.state.file ?
                      (
                        <div style={{ textAlign: "center" }}>
                          <button onClick={this.resetFile}>Remove File</button>
                        </div>
                      ) : null
                    }
                    {this.state.file ? <img style={{ width: "100%" }} src={this.state.file} alt="not found" /> : null}

                  </InputGroup>
                  <InputGroup className="mb-4">
                    <Input
                      type="text"
                      id="search"
                      name="search"
                      placeholder="Enter emoji name"
                      onChange={this.handleChangeSearch}
                      autoComplete="off"
                    />
                  </InputGroup>
                  <Row>
                    <Col xs="6">
                      <Button color="primary" className="px-4" onClick={this.addEmoji}>Add Emoji</Button>
                    </Col>
                  </Row>
                </Form>
              </Col>
            </Row>
          </ModalBody>
        </Modal>

        <Modal isOpen={this.state.modal2} toggle={this.closeModal.bind(this, 'modal2')}>
          <ModalHeader toggle={this.closeModal.bind(this, 'modal2')}>
            Edit Emoji
           </ModalHeader>
          <ModalBody>
            <Row className="justify-content-center">
              <Col md="8">
                <Form name="form" onSubmit={this.handleSubmit}>
                  <InputGroup className="mb-4">
                    <Input
                      type="text"
                      id="search"
                      name="search"
                      placeholder="Enter emoji name"
                      value={this.state.editText}
                      onChange={this.handleChangeEdited}
                      autoComplete="off"
                    />
                  </InputGroup>
                  <Row>
                    <Col xs="6">
                      <Button color="primary" className="px-4" onClick={this.editEmojiData}>Edit Emoji</Button>
                    </Col>
                  </Row>
                </Form>
              </Col>
            </Row>
          </ModalBody>
        </Modal>

        <Modal isOpen={this.state.modal3} toggle={this.closeModal.bind(this, 'modal3')}>
          <ModalHeader toggle={this.closeModal.bind(this, 'modal3')}>
            Show Emoji
           </ModalHeader>
          <ModalBody>
            <Row className="justify-content-center">
              <Col md="8">
              <img src={this.state.showImageURL}  className="img-avatar" alt="Show images" />
              </Col>
            </Row>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

//export default Dashboard;
function mapStateToProps(state) {
  const { dashboard } = state;

  return {
    dashboard
  };
}
export default connect(mapStateToProps)(Dashboard);