import React, { Component } from 'react';

import {
  Card,
  CardBody,
  CardHeader,
  FormGroup,
  Label,
  Button,
  Input,
  Row,
  Col,
  Table,
  Modal,
  ModalHeader,
  ModalBody,
} from 'reactstrap';
import { connect } from 'react-redux';
import { settingActions } from '../../_actions';
import { EditorState, convertToRaw,ContentState } from 'draft-js';

import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import ImageUploader from 'react-images-upload';
import { CONST } from '../../_config';
import axios from 'axios';
import { authHeader } from '../../_helpers';
import { toast } from 'react-toastify';
import moment from 'moment'



class NotificationToUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      content: '',
      editorState: "",
      arrimagename:"",
      notifilist:[],
      descriptionState:""
    }
    this.onChange = (editorState) => this.setState({editorState});
    this.updateSetting = this.updateSetting.bind(this);
    this.handleChangeUpdate = this.handleChangeUpdate.bind(this);
    this.onDropNewPlayer1 = this.onDropNewPlayer1.bind(this);
    this.NotificationList=this.NotificationList.bind(this);
    this.addPoolToggle = this.addPoolToggle.bind(this);
    this.setEditor = (editor) => {
      this.editor = editor;
    };
    this.focusEditor = () => {
      if (this.editor) {
        this.editor.focus();
      }
    };
  }
  componentDidMount() {
    this.focusEditor();
    this.NotificationList();
    this.props.dispatch(settingActions.getstaticpage({slug:'aboutus'}));
  }
 
  onEditorStateChange = (descriptionState) => {
   this.setState({
    descriptionState:descriptionState.target.value,
    });
    //console.log(draftToHtml(convertToRaw(editorState.getCurrentContent())));
  };
  updateSetting(data) {
    let formthis=this;
    console.log("this.state.arrimagename--->>",this.state.arrimagename);
    let imgurl="";
    if(this.state.arrimagename.error===false)
    {
      imgurl=(this.state.arrimagename.data)?this.state.arrimagename.data[0]:"";
    }
    const formData = new FormData();
      formData.append('title', this.state.title);
      formData.append('message',this.state.descriptionState);
      formData.append('img', imgurl);
      formData.append('atype', "add");
      const config = {
        headers: {
          'content-type': 'multipart/form-data',
          Authorization: authHeader().Authorization
        }
      };
      axios.post(CONST.BACKEND_URL + `/api/addnotification`, formData, config).then(response => {
        formthis.NotificationList();
        formthis.setState({
          addPoolModal: !formthis.state.addPoolModal
        });
      })
    //this.props.dispatch(settingActions.updatestaticpage(data1));
  }
  handleChangeUpdate(e) {
    console.log('e.target.id  ', e.target.id);
    console.log('e.target.name  ', e.target.name);
    console.log('e.target.name  ', e.target.value);
    this.setState({ [e.target.name]: e.target.value});
  }

  onDropNewPlayer1(picture) {
    let formthis=this;
    console.log("picture---->>>",picture);
    const formData = new FormData();
    formData.append('images[]', picture[picture.length - 1]);
    formData.append('imgtype', "noti");
    const config = {
      headers: {
          'content-type': 'multipart/form-data',
          'Authorization': authHeader().Authorization
      }
  };
  axios.post(CONST.BACKEND_URL + `/api/uploadmultipleimg`, formData, config)
      //.then(handleResponse)
      .then((response) => {
          //alert("The file is successfully uploaded1111111111111");
          console.log("response   ", response);
          formthis.setState({arrimagename:response.data});
          return response.data;
      })
      .catch((error) => {
          console.log('========================33333333============');
          console.log(error);
          console.log('====================================');
      });
}


NotificationList(){
  let formthis=this;
    //console.log("picture---->>>",picture);
    const formData = new FormData();
    //formData.append('images[]', picture[picture.length - 1]);
    //formData.append('imgtype', "noti");
    const config = {
      headers: {
          'content-type': 'multipart/form-data',
          'Authorization': authHeader().Authorization
      }
  };
  axios.post(CONST.BACKEND_URL + `/api/notificationlist`, formData, config)
      //.then(handleResponse)
      .then((response) => {
          //alert("The file is successfully uploaded1111111111111");
          console.log("response   ", response);
         // dispatch(alertActions.success('Subadmin added !'));
          formthis.setState({notifilist:response.data.data.list});
          return response.data;
      })
      .catch((error) => {
          console.log('========================33333333============');
          console.log(error);
          //dispatch(alertActions.error(error));
          console.log('====================================');
      });
}

addPoolToggle() {
  this.setState({
    addPoolModal: !this.state.addPoolModal,
    title:"",
    editorState:"",
    arrimagename:[],
    descriptionState:""
  });
}


  render() {    
    let {setting}=this.props;
    let {staticpageDetails}=setting;
    //const { editorState } = this.state;
    return (
      <div className="animated fadeIn">
       {staticpageDetails? <Row>
          <Col xl="12">
          
       
            <Card>
            <Modal isOpen={this.state.addPoolModal} toggle={this.addPoolToggle} className={ 'my-modal ' + this.props.className}>
    <ModalHeader toggle={this.addPoolToggle}>
        Add Pool
    </ModalHeader>
    <ModalBody>
              <CardHeader>
                <i className="fa fa-align-justify" /> Notification
                <div className="card-header-actions" />
              </CardHeader>
              <CardBody>
                 <Row>
                  <Col xs="12">
                    <FormGroup>
                      <Label htmlFor="Title">Title*</Label>
                      <Input type="text" id="title" name="title" placeholder="Enter title" value={this.state.title}  onChange={this.handleChangeUpdate}  />
                    </FormGroup>
                  </Col>
                </Row>
                {/* <Row>
                  <Col xs="12">
                    <FormGroup>
                      <Label htmlFor="description">Description</Label>
                      <Input type="textarea" id="content"  name="content" placeholder="Enter description" rows="4"  value={this.state.content}  onChange={this.handleChangeUpdate}  />
                    </FormGroup>
                  </Col>
                </Row> */}
                <Row>
                  <Col xs="12">
                    <FormGroup>
                      <Label htmlFor="description">Description*</Label>
                      {/* <Editor
                        currentState={this.state.editorState}
                        editorState={this.state.editorState}
                        wrapperClassName="home-wrapper"
                        editorClassName="home-editor"
                        toolbar={{
                          fontFamily: {
                            options: ['Arial', 'Georgia', 'Impact', 'Tahoma', 'Roboto', 'Times New Roman', 'Verdana'],
                          }
                        }}
                        onEditorStateChange={this.onEditorStateChange}
                        placeholder=""
                        hashtag={{}}
                      /> */}
                      <textarea  class="form-control" rows="5"
                      value={this.state.descriptionState} 
                      onChange={this.onEditorStateChange}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12">
                  <img src={CONST.BACKEND_URL + '/uploads/teamlogo/'+this.state.notificationPostImage} width="50" height="50" alt=""   /> 
                  <ImageUploader singleImage={true} withIcon={false} buttonText="Add Post" withLabel={false} withPreview={false} onChange={this.onDropNewPlayer1} maxFileSize={5242880} />
                  
                  </Col>
                </Row>
                <Row>
                  <Col xs="12">
                   <Button type="button" color="primary" onClick={()=>this.updateSetting()}>
                            Update
                    </Button>
                  </Col>
                </Row>
                    
              </CardBody>
              </ModalBody>
            </Modal>
            <CardHeader>
                        <FormGroup row>
                          <Col xl="8"><strong><i className="icon-info pr-1"></i> </strong></Col>
                             <Button  onClick={this.addPoolToggle}  className="mr-1"  color="success">
                              Add Notification
                            </Button>
                        </FormGroup>
                      </CardHeader>
              <CardBody>
              <Table responsive striped hover >
              <thead>
                  <tr>
                    <th>Title</th>
                    <th>created</th>
                  </tr>
                  </thead>
                  <tbody>
                   {
                     this.state.notifilist.map(function(item,index){
                      return(<tr key={index}>
                      <td>{item.title}</td>
                      <td>{moment(new Date(parseInt(item.created)*1000)).utcOffset("+05:30").format("YYYY-MM-DD")}</td>
                      </tr>)
                     })
                   }
                   </tbody>
                 </Table>
              </CardBody>
            </Card>
           
          </Col>
        </Row> :null
        
        }
      </div>
    );
  }
}
function mapStateToProps(state) {
  const { setting, authentication } = state;
  const { user } = authentication;
  return {
    user,
    setting
  };
}
export default connect(mapStateToProps)(NotificationToUser);
