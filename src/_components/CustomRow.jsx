import React from 'react';
//import { Route, Redirect } from 'react-router-dom';
import {Button} from 'reactstrap';
import { connect } from 'react-redux';
class CustomRow extends React.Component {

    shouldComponentUpdate(nextProps) {
        if (this.props.message === nextProps.message) {
            return false
        }

        return true
    }
    render() {
        return (
            <tr >
                {/* <td>asldkfjalskjfdl</td> */}
                <th scope="row">{this.props.index}</th>
                <td><img src={this.props.imojiData.imageURL} style={{ height: '50px', width: '50px' }}
                    onClick={() => this.showImageEmoji(this.props.imojiData)}
                    className="img-avatar" alt="bootstrapmaster" /></td>
                <td>{this.props.imojiData.imojiName}</td>
                <td>
                    {
                        this.props.imojiData.isEnable ?
                            <Button color="danger" onClick={() => this.props.updateStatus(this.props.imojiData)} style={{ marginRight: '10px' }}>
                                Disable</Button> :
                            <Button color="success" onClick={() => this.props.updateStatus(this.props.imojiData)} style={{ marginRight: '10px' }}>Enable</Button>
                    }
                    {'  '}
                    <Button color="warning"
                        onClick={() => this.editEmoji(this.props.imojiData)}
                        style={{ marginRight: '10px' }}>Edit</Button>
                    {'  '}
                    <Button color="danger" onClick={() => this.deleteEmoji(this.props.imojiData)} style={{ marginRight: '10px' }}>Delete</Button>
                </td>
            </tr>
        )
    }
}
function mapStateToProps(state) {
    const { dashboard } = state;
  
    return {
      dashboard
    };
}
// const mapDispatchToProps = dispatch => {
//     return {
//         updateStatus: data => {
//         dispatch(this.props.updateStatus(data))
//       }
//     }
// }
 export default connect(mapStateToProps)(CustomRow);
// export const Row = ({ component: Component, ...rest }) => (
//     <Route {...rest} render={props => (
//         localStorage.getItem('user')
//             ? <Component {...props} />
//              : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
//     )} />
// )